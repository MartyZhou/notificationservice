FROM microsoft/dotnet:2.1-aspnetcore-runtime AS base
WORKDIR /app
EXPOSE 34660
EXPOSE 44351

FROM microsoft/dotnet:2.1-sdk AS build
WORKDIR /src
COPY ["Seismic.Notification.Api/Seismic.Notification.Api.csproj", "Seismic.Notification.Api/"]
COPY ["Seismic.Notification.Domain/Seismic.Notification.Domain.csproj", "Seismic.Notification.Domain/"]
COPY ["Seismic.Notification.Storage/Seismic.Notification.Storage.csproj", "Seismic.Notification.Storage/"]
COPY ["Seismic.Notification.Core/Seismic.Notification.Core.csproj", "Seismic.Notification.Core/"]
RUN dotnet restore "Seismic.Notification.Api/Seismic.Notification.Api.csproj"
COPY . .
WORKDIR "/src/Seismic.Notification.Api"
RUN dotnet build "Seismic.Notification.Api.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "Seismic.Notification.Api.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "Seismic.Notification.Api.dll"]