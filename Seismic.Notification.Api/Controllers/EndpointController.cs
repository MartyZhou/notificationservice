﻿using Microsoft.AspNetCore.Mvc;
using Seismic.Notification.Core.Services;
using Seismic.Notification.Domain.Requests;
using Seismic.Notification.Domain.Resources;
using System.Threading.Tasks;

namespace Seismic.Notification.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EndpointController : ControllerBase
    {
        private readonly IRegistrationService registrationService;

        public EndpointController(IRegistrationService registrationService)
        {
            this.registrationService = registrationService;
        }

        [HttpGet("tenant/{tenant}/{id}", Name = "Get")]
        public async Task<ActionResult<RegistrationResource>> GetAsync(string tenant, string id)
        {
            var result = await registrationService.FindEndpointAsync(tenant, id).ConfigureAwait(false);
            return Ok(result);
        }

        [HttpPost]
        public async Task<ActionResult<RegistrationResource>> Post([FromBody] RegistrationRequest endpoint)
        {
            var result = await registrationService.InstallEndpointAsync(endpoint).ConfigureAwait(false);
            return Ok(result);
        }

        [HttpPut("{id}")]
        public ActionResult<RegistrationResource> Put(string id, [FromBody] RegistrationRequest endpoint)
        {

            return NotFound();
        }

        [HttpDelete("tenant/{tenant}/{id}")]
        public async Task<ActionResult> Delete(string tenant, string id)
        {
            await registrationService.DeleteEndpointAsync(tenant, id).ConfigureAwait(false);
            return Ok();
        }
    }
}
