﻿using Seismic.Notification.Domain.Domains;
using System.Collections.Generic;

namespace Seismic.Notification.Domain.Resources
{
    public class RegistrationResource
    {
        public string Id { get; set; }

        public string EndpointId { get; set; }

        public EndpointPlatform Platform { get; set; }

        public IDictionary<string, string> CustomFields { get; set; }
    }
}
