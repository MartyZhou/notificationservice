﻿using Seismic.Notification.Domain.Domains;
using System.Collections.Generic;

namespace Seismic.Notification.Domain.Requests
{
    public class MessageRequest
    {
        public string Tenant { get; set; }

        public IList<EndpointPlatform> Targets { get; set; }

        public IDictionary<string, string> CustomFields { get; set; }
    }
}
