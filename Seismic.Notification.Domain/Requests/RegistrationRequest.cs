﻿using Seismic.Notification.Domain.Domains;
using System.Collections.Generic;

namespace Seismic.Notification.Domain.Requests
{
    public class RegistrationRequest
    {
        public string UserId { get; set; }

        public string UserName { get; set; }

        public IDictionary<string, string> Tubes { get; set; }

        public string EndpointId { get; set; }

        public string Tenant { get; set; }

        public EndpointPlatform Platform { get; set; }
    }
}
