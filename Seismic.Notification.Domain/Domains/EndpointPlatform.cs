﻿namespace Seismic.Notification.Domain.Domains
{
    public enum EndpointPlatform
    {
        NCApns,
        NCGcm,
        WSApns,
        WSGcm,
        DCApns,
        DCGcm,
        SignalR,
        Email
    }
}
