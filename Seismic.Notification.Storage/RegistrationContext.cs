﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Seismic.Notification.Domain.Domains;

namespace Seismic.Notification.Storage
{
    public class RegistrationContext : DbContext
    {
        public RegistrationContext(DbContextOptions<RegistrationContext> options) : base(options) { }

        public DbSet<RegistrationEntity> RegistrationEntities { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var converter = new EnumToStringConverter<EndpointPlatform>();

            modelBuilder
                .Entity<RegistrationEntity>()
                .ToTable("DeviceRegistration", "S_nhubdev")
                .Property(e => e.Platform)
                .HasConversion(converter);
        }
    }
}
