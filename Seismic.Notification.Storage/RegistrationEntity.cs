﻿using Seismic.Notification.Domain.Domains;
using System;
using System.ComponentModel.DataAnnotations;

namespace Seismic.Notification.Storage
{
    public class RegistrationEntity
    {
        public string Id { get; set; }

        [Key]
        public string PNSHandle { get; set; }

        public string UserId { get; set; }

        public string Tenant { get; set; }

        public EndpointPlatform Platform { get; set; }

        public DateTime CreatedTime { get; set; }

        public DateTime LastUpdated { get; set; }
    }
}
