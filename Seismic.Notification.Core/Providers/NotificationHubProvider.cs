﻿using Microsoft.Azure.NotificationHubs;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace Seismic.Notification.Core.Providers
{
    public class NotificationHubProvider : IMessageProvider
    {
        private readonly NotificationHubClient hub;

        public NotificationHubProvider(IConfiguration config)
        {
            var hubName = config["HubName"];
            var hubConnection = config["HubConnectionFullAccess"];
            hub = NotificationHubClient.CreateClientFromConnectionString(hubConnection, hubName);
        }

        public Task<Installation> GetInstallationAsync(string id)
        {
            return hub.GetInstallationAsync(id);
        }

        public Task CreateOrUpdateInstallationAsync(Installation install)
        {
            return hub.CreateOrUpdateInstallationAsync(install);
        }

        public Task DeleteInstallationAsync(string id)
        {
            return hub.DeleteInstallationAsync(id);
        }

        public Task<CollectionQueryResult<RegistrationDescription>> GetAllRegistrationsAsync(int top)
        {
            return hub.GetAllRegistrationsAsync(top);
        }

    }
}
