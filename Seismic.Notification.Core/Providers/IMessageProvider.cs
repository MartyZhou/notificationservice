﻿using Microsoft.Azure.NotificationHubs;
using System.Threading.Tasks;

namespace Seismic.Notification.Core.Providers
{
    public interface IMessageProvider
    {
        Task<Installation> GetInstallationAsync(string id);

        Task CreateOrUpdateInstallationAsync(Installation install);

        Task DeleteInstallationAsync(string id);

        Task<CollectionQueryResult<RegistrationDescription>> GetAllRegistrationsAsync(int top);
    }
}
