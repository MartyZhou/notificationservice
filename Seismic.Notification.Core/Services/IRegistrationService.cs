﻿using Seismic.Notification.Domain.Requests;
using Seismic.Notification.Domain.Resources;
using System.Threading.Tasks;

namespace Seismic.Notification.Core.Services
{
    public interface IRegistrationService
    {
        Task<RegistrationResource> FindEndpointAsync(string tenant, string endpointId);

        Task<RegistrationResource> InstallEndpointAsync(RegistrationRequest endpoint);

        Task DeleteEndpointAsync(string tenant, string endpointId);

    }
}
