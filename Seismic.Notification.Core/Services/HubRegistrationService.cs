﻿using Microsoft.Azure.NotificationHubs;
using Seismic.Notification.Core.Providers;
using Seismic.Notification.Domain.Requests;
using Seismic.Notification.Domain.Resources;
using Seismic.Notification.Storage;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Seismic.Notification.Core.Services
{
    public class HubRegistrationService : IRegistrationService
    {
        private readonly IMessageProvider messageProvider;
        private readonly RegistrationContext registrationContext;

        public HubRegistrationService(IMessageProvider messageProvider, RegistrationContext registrationContext)
        {
            this.messageProvider = messageProvider;
            this.registrationContext = registrationContext;
        }


        public async Task<RegistrationResource> FindEndpointAsync(string tenant, string endpointId)
        {
            var entity = registrationContext.Find<RegistrationEntity>(endpointId);

            var install = await messageProvider.GetInstallationAsync(entity.Id).ConfigureAwait(false);

            var result = new RegistrationResource
            {
                Id = entity.Id,
                EndpointId = entity.PNSHandle,
                Platform = entity.Platform
            };

            var customFields = new Dictionary<string, string>();
            customFields.Add("Tags", string.Join(',', install.Tags));

            result.CustomFields = customFields;

            return result;
        }

        public async Task<RegistrationResource> InstallEndpointAsync(RegistrationRequest endpoint)
        {
            var install = new Installation();
            install.InstallationId = Guid.NewGuid().ToString();
            install.Platform = NotificationPlatform.Gcm;
            install.PushChannel = endpoint.EndpointId;

            var tags = new List<string>();
            tags.Add(string.Format("UserId_{0}", endpoint.UserId));
            foreach (var tube in endpoint.Tubes)
            {
                tags.Add(string.Format("ChannelId_{0}", tube.Key));
            }

            install.Tags = tags;

            await messageProvider.CreateOrUpdateInstallationAsync(install).ConfigureAwait(false);

            var regEntity = new RegistrationEntity
            {
                Id = install.InstallationId,
                Platform = Domain.Domains.EndpointPlatform.NCGcm,
                PNSHandle = install.PushChannel,
                Tenant = endpoint.Tenant,
                UserId = endpoint.UserId,
                CreatedTime = DateTime.UtcNow,
                LastUpdated = DateTime.UtcNow
            };

            registrationContext.Add<RegistrationEntity>(regEntity);
            await registrationContext.SaveChangesAsync().ConfigureAwait(false);

            var result = new RegistrationResource
            {
                Id = regEntity.Id,
                EndpointId = regEntity.PNSHandle,
                Platform = regEntity.Platform
            };

            var customFields = new Dictionary<string, string>();
            customFields.Add("Tags", string.Join(',', install.Tags));

            result.CustomFields = customFields;

            return result;
        }

        public async Task DeleteEndpointAsync(string tenant, string endpointId)
        {
            var entity = registrationContext.Find<RegistrationEntity>(endpointId);

            await messageProvider.DeleteInstallationAsync(entity.Id).ConfigureAwait(false);

            registrationContext.Remove<RegistrationEntity>(entity);
            await registrationContext.SaveChangesAsync().ConfigureAwait(false);
        }
    }
}
