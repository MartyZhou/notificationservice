﻿using Seismic.Notification.Core.Providers;
using Seismic.Notification.Core.Services;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class MessageExtensions
    {
        public static IServiceCollection AddNotificationHubProvider(this IServiceCollection services)
        {
            services.AddSingleton<IMessageProvider, NotificationHubProvider>();
            services.AddScoped<IRegistrationService, HubRegistrationService>();
            // services.AddScoped<IMessageProvider, NotificationHubProvider>();

            return services;
        }
    }
}
